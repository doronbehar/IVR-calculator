#!/usr/bin/env python

from pathlib import Path
import argparse

import matplotlib.pyplot as plt
PLT_COLORS = plt.rcParams["axes.prop_cycle"].by_key()["color"]
import numpy as np

parser = argparse.ArgumentParser(
    description='Plot list of energies combinations',
)
parser.add_argument(
    'combos_files',
    help='.npz files generated by generate-IVR-data.py script, each containing all energies combinations',
    nargs='+',
    type=argparse.FileType('r'),
)
parser.add_argument(
    '-b', '--bins',
    help='The number of bins to use when computing the histogram (defaults to sqrt(len(energies)))',
    nargs=1,
    default=[-1],
    type=int,
)
parser.add_argument(
    '--xticks',
    help='Enable/Disable showing xticks of omega_# (shows by default)',
    action=argparse.BooleanOptionalAction,
    default=True,
)

args = parser.parse_args()
fig, ax = plt.subplots()
for combos_file_idx,combos_file in enumerate(args.combos_files):
    combos_path = Path(combos_file.name)
    data_name = str(combos_path.with_suffix('').with_suffix('').name)

    data = np.load(combos_path)
    energies = data['energies']
    omegas = data['eigen_energies']
    secAxis = ax.secondary_xaxis(location='bottom')
    if args.xticks:
        secAxis.set_xticks(
            omegas,
            labels=[r'{}$\omega_{{{}}}$'.format(
               "\n"*(combos_file_idx+1),
                oidx
            ) for oidx in range(len(omegas))],
            color=PLT_COLORS[combos_file_idx],
        )
        for omega in omegas:
            ax.axvline(omega, linestyle='--', color='k', alpha=0.2)

    if args.bins[0] < 0:
        bins = round(np.sqrt(len(energies))) # Heuristic
        print("{}:\t Using {} bins for histogram".format(
            combos_file.name,
            bins,
        ))
        #args.bins[0] = round(max(energies) - min(energies)) # Less heuristic Perhaps
    hist, bin_edges = np.histogram(energies, bins=bins)
    hist_normalized = hist/np.mean(np.diff(bin_edges))
    ax.plot(
        bin_edges[1:],
        hist_normalized,
        label="{}, tot = {}".format(data_name, len(energies)),
    )

plt.title('Energy combos per wavenumber')
plt.ylabel('[$\# \cdot cm$]')
plt.xlabel('{}Energy [$cm^{{-1}}$]'.format("\n"*(combos_file_idx+1)*int(args.xticks)))
plt.tight_layout()
# This will always be the best place, due to the nature of our data
plt.legend(loc='upper left')
plt.show()
