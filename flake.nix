{
  description = "Flake for computations of homework assignments";

  # To make user overrides of the nixpkgs flake not take effect
  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

  outputs = { self
  , nixpkgs
  }: let
    pkgs = import nixpkgs {
      system = "x86_64-linux";
      overlays = [
      ];
    };
    pyPkgs = p: [
      p.numpy
      p.matplotlib
      p.progressbar2
      p.tabulate

      p.pyqt6

      # For text editor
      p.jedi-language-server
      p.debugpy
    ];
    python = pkgs.python3.override {
      packageOverrides = selfPython: superPython: {
      };
    };
    pythonEnv = python.withPackages(pyPkgs);
    # Meant for compiling latex text in matplotlib. Using Latex text in plots
    # requires the obscure latex package type1ec, see:
    # https://github.com/matplotlib/matplotlib/issues/16911
    texlive = pkgs.texlive.combine {
      inherit (pkgs.texlive)
        scheme-basic
        cm-super
        type1cm
        underscore
        dvipng
        xetex
        fontspec
      ;
    };
  in {
    devShell.x86_64-linux = pkgs.mkShell {
      nativeBuildInputs = [
        pythonEnv
        # for matplotlib gtk3 backend
        pkgs.gtk3
        pkgs.gobject-introspection
        texlive
      ];
      QT_PLUGIN_PATH = pkgs.lib.pipe [
        pkgs.qt6.qtbase
        pkgs.qt6.qtwayland
      ] [
        (map (p: "${pkgs.lib.getBin p}/${pkgs.qt6.qtbase.qtPluginPrefix}"))
        (pkgs.lib.concatStringsSep ":")
      ];
      QT_QPA_PLATFORM="wayland";
      XDG_DATA_DIRS = pkgs.lib.concatStringsSep ":" [
        # So we'll be able to save figures from the plot dialog
        "${pkgs.gsettings-desktop-schemas}/share/gsettings-schemas/${pkgs.gsettings-desktop-schemas.name}"
        "${pkgs.gtk3}/share/gsettings-schemas/${pkgs.gtk3.name}"
        # So pdf mime types and perhaps others could be detected by 'gio open'
        # / xdg-open. TODO: Is this the best way to overcome this issue -
        # manually every time we generate a devShell?
        "${pkgs.shared-mime-info}/share"
        "${pkgs.hicolor-icon-theme}/share"
      ];
      GDK_PIXBUF_MODULE_FILE = "${pkgs.librsvg}/${pkgs.gdk-pixbuf.moduleDir}.cache";
    };
    packages.x86_64-linux = {
      inherit
        texlive
        pythonEnv
      ;
    };
  };
}
