#!/usr/bin/env python

from pathlib import Path

import sys
import argparse
from shutil import get_terminal_size
from itertools import product

# **progressbar2**: URL: https://progressbar-2.readthedocs.io/
import progressbar
import numpy as np

parser = argparse.ArgumentParser(
    description='Generate list of energies combinations',
)
parser.add_argument(
    'energies_file',
    help='A file with a list of all Eigen energies',
    nargs=1,
    type=argparse.FileType('r'),
    default=sys.stdin
)
parser.add_argument(
    '--max-energy',
    help="Maximal energy to cut iterations when reaching it, defaults to a heuristic decision",
    nargs=1,
    required=False,
)
args = parser.parse_args()
fname = args.energies_file[0].name
omegas = []
for omega in args.energies_file[0].read().splitlines():
    try:
        omegas.append(float(omega))
    except TypeError:
        exit("Energy {} in given energies_file {} doesn't convert to float".format(
            omega,
            fname
        ))
omegas = np.sort(omegas)

print("Energies are {}".format(omegas))

# Will run all the loops below loop up until reaching this bound
if args.max_energy is None:
    energy_bound = omegas[-1]+omegas[0]*3
    computed = "(cumputed)"
else:
    energy_bound = args.max_energy
    computed = ""
print("Cutoff energy {} used will be {}".format(computed, energy_bound))

savePath = Path(fname).resolve().with_suffix('.combinations.npz')
print("Will save all combinations to {}".format(savePath.relative_to(Path.cwd())))

# We model the problem as follows:
#
# - Every omega is a vector, with len(omegas) elements.
# - The total energy, is a linear combination of one the vectors above.
# - The coefficients for the linear combinations are all positive integers.
# - The total energy of such a vector is the sum of all of it's elements.
#
# Example for a vector:
#
# ⎡1⎤
# ⎢0⎥
# ⎢2⎥
# ⎢5⎥
# ⎢0⎥ => Energy = ω_1*1 + ω_3*2 + ω_4*5
# ⎢0⎥
# ⎢.⎥
# ⎢.⎥
# ⎢.⎥
# ⎣0⎦

# Each `vector` in the `product` for loop below, is the set of coefficients as
# described above. This iteration is based on:
# https://stackoverflow.com/a/57149550/4935114
#
# We need to limit the generation of these positive integers vectors, before
# iterating _all_ possible vectors. Hence we first compute the maximal
# coefficient per ω, that would not give us an energy higher then
# `energy_bound`.
vector_of_max_coefficients = []
for omega in omegas:
    n = 1
    while omega*n <= energy_bound:
        n+=1
    vector_of_max_coefficients.append(n)
tot = get_terminal_size((80,0)).columns
intro = "maximal coefficients are:"
def format_tuple(t):
    out = "("
    fill_size = len(str(max(vector_of_max_coefficients)))
    for i,x in enumerate(t):
        if i == len(t)-1:
            comma = ")"
        else:
            comma = ", "
        out+=str(x).zfill(fill_size)+comma
    return out
# An additional space is needed due to progressbar printing such space as well
outro = "ω = " + format_tuple(vector_of_max_coefficients) + " "
padding = tot - len(intro) - len(outro)
print("{}{}".format(intro.ljust(padding + len(intro)), outro))
try:
    combos = []
    energies = []
    with progressbar.ProgressBar(
        max_value=progressbar.UnknownLength,
        widgets=[
            ' [', progressbar.Timer(), '] ',
            progressbar.Bar(),
            progressbar.Variable('combination'),
        ],
    ) as bar:
        for vidx,vector in enumerate(product(*[range(x + 1) for x in vector_of_max_coefficients])):
            bar.update(vidx)
            energy = np.dot(vector, omegas)
            if energy < energy_bound:
                energies.append(energy)
                combos.append(vector)
                bar.update(vidx, combination="{}".format(format_tuple(vector)))
except KeyboardInterrupt:
    exit()

np.savez(
    savePath,
    combos=combos,
    energies=energies,
    eigen_energies=omegas,
    max_energy=energy_bound
)
print("Finished!")
